#include <stdio.h>
#include <stdlib.h>

int print_flag()
{
    FILE *fd = fopen("/pwn/flag.txt", "r");
    char flag[32];
    fgets(flag, 32, fd);
    printf("%s\n", flag);
    fclose(fd);
    return 0;
}

int vuln()
{
    int var;
    int overwrite_me = 0xc00ffee;
    char buf[1000];

    printf("Bienvenue dans Word 2.0!\n");
    printf("L'edition word specialisee dans l'edition de code !\n");
    printf("Pourquoi ne pas commencer son TP Java maintenant ? A toi de jouer !\n");
    printf("public class Pwn {\n\n\tpublic static void main(String[] args) {\n\t\t");
    fflush(stdout);
    fgets(buf, 1005, stdin);
    
    if(overwrite_me == 0x41414141) {
        printf("Tu as du oublier la conception methodique...\n");
        fflush(stdout);
    } else if (overwrite_me == 0x20babe20) {
        printf("Quel boss ! TestAlgo se retourne dans sa tombe !\n");
        printf("Voila le flag: ");
        fflush(stdout);
        print_flag();
    } else {
        printf("Pas mal... Ca vaut au max 5/20, peut mieux faire\n");
        fflush(stdout);
    }
    return 0;
}

int main() 
{
	vuln();
	return 0;
}
