#include <stdio.h>

int print_flag()
{                                                                                                                                                                             
	FILE *fd = fopen("/pwn/flag.txt", "r");                                                                                                                                                      
    char flag[32];                                                                                                                                                                            
    fgets(flag, 32, fd);                                                                                                                                                                      
    printf("%s\n", flag);                                                                                                                                                                     
	fclose(fd);                                                                                                                                                                               
    return 0;                                                                                                                                                                                 
} 

int vuln() 
{
	int var;
	int check;
	char name[128];

	printf("Hello ! What's your name ?\n");
	fflush(stdout);
	fgets(name, 133, stdin);
	printf("Hello %s\n", name);
	if(check == 0x41414141) {
		printf("Nice ! You win !\nHere's the flag: ");
		fflush(stdout);
		print_flag();
	}
	return 0;
}

int main() 
{
	vuln();
	return 0;
}
