#include <stdio.h>

int win() {
    system("/bin/sh");
}

int vuln()
{
    char buf[40];

    printf("Sauras-tu defier mon control flow imparable ?\n");
    fflush(stdout);
    scanf("%s", &buf);
    return 0;
}

int main() 
{
    vuln();
    return 0;
}
