#include <stdio.h>
#include <string.h>


int main()
{
    char user_code[4];
    char code[4]; 
    char name[16];
    FILE *random = fopen("/dev/urandom", "r");
    
    fgets(code, 4, random);

    printf("What's your name ? ");
    fflush(stdout);
    fgets(name, 16, stdin);
    printf("Hello ");
    printf(name);
    fflush(stdout);

    printf("\nWhat is the code ?\n");
    fflush(stdout);
    fgets(user_code, 4, stdin);
    
    if(strcmp(user_code, code) == 0)
    {
        printf("Welcome back sir, shell opened.\n");
        system("/bin/sh");
    } 
    else 
    {
        printf("Access Denied.\n");
    }
    return 0;
}