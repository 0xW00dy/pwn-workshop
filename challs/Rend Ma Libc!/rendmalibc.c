#include <stdio.h>


int vuln()
{
    char buf[200];

    printf("Quelle est ta libc préférée ?");
    fflush(stdout);
    gets(buf);
    printf("Cool ! Moi c'est la libc.so.6 ! Tu peux la retrouver sur le site ctf.woody.sh ;)\n");
    fflush(stdout);
    
    return 0;
}

int main()
{
    vuln();
    return 0;
}