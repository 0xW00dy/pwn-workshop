#include <stdio.h>
#include <stdlib.h>

int print_flag()
{
    FILE *fd = fopen("/pwn/flag.txt", "r");
    char flag[32];
    fgets(flag, 32, fd);
    printf("Here's the flag ! %s\n", flag);
    fclose(fd);
    return 0;
}

int main() 
{
    int var;
    int check = 0xbabecafe;
    int canary = 0xf00dbabe;
    char buf[1337];

    printf("Hey.. how are you ? UwU\n");
    fflush(stdout);
    fgets(buf, 1346, stdin);

    if(canary != 0xf00dbabe) {
        printf("You're stealing me food ?!?! Go away !\n");
    } else if(check == 0xda7ebabe){
        printf("Going on a date ? Yes !\n");
        print_flag();
    } else {
        printf("I'm fine.. thanks OwO\n");
    }
    fflush(stdout);
    return 0;
}