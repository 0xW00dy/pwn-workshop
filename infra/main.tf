terraform {
	required_providers {
	  docker = {
		source = "kreuzwerker/docker"
		version = "2.15.0"
	  }
	}
}

#### Images

resource "docker_image" "writeeee" {
	name = "writeeee"
	keep_locally = true 
}

resource "docker_image" "yourcrush" {
	name = "yourcrush"
	keep_locally = true
}

resource "docker_image" "startingpoint" {
	name = "startingpoint"
	keep_locally = true
}

resource "docker_image" "skipro" {
	name = "skipro"
	keep_locally = true
}

resource "docker_image" "rendmalibc" {
	name = "rendmalibc"
	keep_locally = true
}

resource "docker_image" "master" {
	name = "master"
	keep_locally = true
}

resource "docker_image" "poireau" {
	name = "poireau"
	keep_locally = true
}

#### Containers

resource "docker_container" "startingpoint" {
	name = "startingpoint-chall"
	image = docker_image.startingpoint.name

	ports {
		internal = 2000
		external = 2000
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "writeeee" {
	name = "writeeee-chall"
	image = docker_image.writeeee.name

	ports {
		internal = 2001
		external = 2001
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "yourcrush" {
	name = "yourcrush-chall"
	image = docker_image.yourcrush.name
	
	ports {
		internal = 2002
		external = 2002
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "skipro" {
	name = "skipro-chall"
	image = docker_image.skipro.name

	ports {
		internal = 2003
		external = 2003
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "rendmalibc" {
	name = "rendmalibc-chall"
	image = docker_image.rendmalibc.name

	ports {
		internal = 2004
		external = 2004
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "master" {
	name = "master-chall"
	image = docker_image.master.name

	ports {
		internal = 2005
		external = 2005
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

resource "docker_container" "poireau" {
	name = "poireau-chall"
	image = docker_image.poireau.name

	ports {
		internal = 2006
		external = 2006
		ip = "0.0.0.0"
		protocol = "tcp"
	}
}

